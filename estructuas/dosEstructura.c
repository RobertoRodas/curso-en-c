#include<stdio.h>
#include<string.h>

void vacia_buffer();

#define MAX_STRING 128

struct nota {
    float nota1;
    float nota2;
    float nota3;
    float promedio;
};

struct alumno {
    char nombre[MAX_STRING];
    char sexo[MAX_STRING];
    int edad;
    struct nota prom;
};


int main() {

    int aMenor = 0, aMayor = 0;
    float pMenor = 0, pMayor = 0;
    int n = 0, i;

    printf("Cuantos alumnos vas a evaluar: ");
    scanf("%i", &n);

    struct alumno alumnos[n];

    for (i = 0; i < n; i++) {
        vacia_buffer();
        printf("\n%i. Ingrese el nombre del Alumno: ", i + 1);
        fgets(alumnos[i].nombre, MAX_STRING, stdin);
        
        printf("\n%i. Ingrese la edad del Alumno: ", i + 1);
        scanf("%i", &alumnos[i].edad);    
        
        vacia_buffer();

        printf("\n%i. Ingrese sexo del Alumno: ", i + 1);
        fgets(alumnos[i].sexo, MAX_STRING, stdin);

        printf("\n%i. Ingrese las notas del Alumno: ", i + 1);
        scanf("%f %f %f", &alumnos[i].prom.nota1, &alumnos[i].prom.nota2, &alumnos[i].prom.nota3);

        alumnos[i].prom.promedio = (alumnos[i].prom.nota1 + alumnos[i].prom.nota2 + alumnos[i].prom.nota3) / 3;

        pMenor += alumnos[i].prom.promedio;
    }

    for (i = 0; i < n; i++) {
        if (alumnos[i].prom.promedio >= pMayor) {
            pMayor = alumnos[i].prom.promedio;
            aMayor = i;
        } 
        if (alumnos[i].prom.promedio < pMenor) {
            pMenor = alumnos[i].prom.promedio;
            aMenor = i;
        }
        
    }


    printf("\nAlumno Mayor");
    printf("\nEl nombre del Alumno: %s", alumnos[aMayor].nombre);
    printf("\nLa edad del Alumno: %i", alumnos[aMayor].edad);
    printf("\nEl sexo del Alumno: %s", alumnos[aMayor].sexo);     
    printf("\nEl promedio del Alumno: %.2f", alumnos[aMayor].prom.promedio);
    

    printf("\nAlumno Menor");
    printf("\nEl nombre del Alumno: %s", alumnos[aMenor].nombre);
    printf("\nLa edad del Alumno: %i", alumnos[aMenor].edad);
    printf("\nEl sexo del Alumno: %s", alumnos[aMenor].sexo);     
    printf("\nEl promedio del Alumno: %.2f", alumnos[aMenor].prom.promedio);
    

    printf("\n\n");


    //system("pause");

    return 0;
}


void vacia_buffer(){
    int ch;
    while ((ch = getchar()) != '\n' && ch != EOF);
}