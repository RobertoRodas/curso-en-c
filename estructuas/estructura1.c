#include<stdio.h>

void vacia_buffer();

#define MAX_NOMBRE 128

struct persona {
    char nombre[MAX_NOMBRE];
    int edad;
} personas[5] ;


int main() {

    int i = 0;
    for (i = 0; i < 5; i++) {
        
        printf("%i. Ingrese el nombre de persona: ", i + 1);
        fgets(personas[i].nombre, MAX_NOMBRE, stdin);

        
        printf("\n%i. Ingrese la edad de la persona: ", i + 1);
        scanf("%i", &personas[i].edad);    
        
        vacia_buffer();
    }
    
    printf("\n\n");

    for (i = 0; i < 5; i++) {
        printf("\n%iSu nombres es: %s", i + 1, personas[i].nombre);
        printf("\n%iSu edad es: %i", i + 1, personas[i].edad);
    }
    
    printf("\n\n");

    return 0;
}


void vacia_buffer(){
    int ch;
    while ((ch = getchar()) != '\n' && ch != EOF);
}