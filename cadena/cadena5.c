#include<stdio.h>

int main() {
    int c;

    int a = 0;
    int e = 0;
    int i = 0;
    int o = 0;
    int u = 0;
    // && a != 0
    while (EOF != (c = getchar())) {
        switch (c) {
        case 'a':
            a++;
            printf("\nVocales A: %i \n", a);        
            break;
        case 'e':
            e++;
            printf("\nVocales E: %i \n", e);
            break;
        case 'i':
            i++;
            printf("\nVocales I: %i \n", i);
            break;
 
        case 'o':
            o++;
            printf("\nVocales O: %i \n", o);
            break;
 
        case 'u':
            u++;
            printf("\nVocales U: %i \n", u);
            break;
        }
        putchar(c);
    }
    fflush(stdin);

 
    printf("\n\n");
    return 0;
}