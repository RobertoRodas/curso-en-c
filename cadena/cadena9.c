#include<stdio.h>
#include<string.h>

int main() {
    char cadena1[] = "Hola";
    char cadena2[] = "Hola a";

    if (strcmp(cadena1, cadena2) == 0) {
        printf("Las cadenas son iguales");
    }
    else {
        printf("Las cadenas son diferentes");
    }
    
    printf("\n\n");
    return 0;
}