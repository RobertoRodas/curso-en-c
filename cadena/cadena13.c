#include<stdio.h>
//#include<string.h>
#include <ctype.h>

int main() {

    char cadena1[] = "Hola";
    char cadena2[] = "HOLA";

    for(int i = 0; cadena1[i]; i++) 
        cadena1[i] = toupper(cadena1[i]);

    for(int i = 0; cadena2[i]; i++) 
        cadena2[i] = tolower(cadena2[i]);

    printf("%s",cadena1);
    printf("\n");
    printf("%s",cadena2);

    printf("\n\n");
    return 0;
}