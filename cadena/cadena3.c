#include<stdio.h>

int main() {
    int c;
    int i = 0;
    while (EOF != (c = getchar())) {
        if (' ' == c) {
            i++;
        }
        putchar(c);
    }
    fflush(stdin);
    
    printf("\nEspacios: %i \n", i);

    printf("\n\n");
    return 0;
}