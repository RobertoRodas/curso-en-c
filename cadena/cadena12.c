#include<stdio.h>
#include<string.h>

char *strrev(char *str);

int main() {
    char cadena[] = "Hola";

    strrev(cadena);

    printf("%s",cadena);

    printf("\n\n");
    return 0;
}

 char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}