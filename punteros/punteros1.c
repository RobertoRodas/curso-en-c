#include<stdio.h>

int main() {

    int numero = 40;
    int *p_numero;

    p_numero = &numero;

    printf("Valor de la variable");
    printf("\nDato: %i ", numero);
    printf("\nDato: %i ", *p_numero);

    printf("\n\nPosicion de memoria");
    printf("\nPosicion: %p", &numero);
    printf("\nPosicion: %p", p_numero);

    printf("\n\n");

    return 0;
}