#include<stdio.h>

int main() {
    int n[] = {1, 2, 4, 4, 5, 6, 3, 8, 8, 10};
    int *p_n;

    p_n = n;

    for (int i = 0; i < 10; i++) {
        printf("\nDato n[%i]: %i", i, *p_n);
        printf("\nPosicion %p ", p_n);
        printf("\n");
        p_n++;
    }
    
}