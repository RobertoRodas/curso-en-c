#include<stdio.h>
#define MAX_STRING 128

void contarVocales(char *);

int main() {

    char texto[MAX_STRING];

    printf("Ingrese una oracion: ");
    fgets(texto, MAX_STRING, stdin);

    printf("\nOracicon: %s ", texto);
    contarVocales(texto);

    return 0;
}

void contarVocales(char *s) {
    int a = 0;
    int e = 0;
    int i = 0;
    int o = 0;
    int u = 0;

    while (*s) {
        switch (*s) {
            case 'a':
            case 'A':
                a++;
            break;
            case 'e':
            case 'E':
                e++;
            break;
            case 'i':
            case 'I':
                i++;
            break;
            case 'o':
            case 'O':
                o++;
            break;
            case 'u':
            case 'U':
                u++;
            break;
        }
        s++;
    }

    
    printf("\n A %i", a);
    printf("\n E %i", e);
    printf("\n I %i", i);
    printf("\n O %i", o);
    printf("\n U %i", u);
    printf("\n\n");
    
}