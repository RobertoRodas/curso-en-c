#include<stdio.h>
#include<math.h>


int main() {
    float cateto1;
    float cateto2;
    float hipotenusa;

    printf("Ingrese dos digitos: ");
    scanf("%f %f", &cateto1, &cateto2);

    hipotenusa = sqrt(pow(cateto1,2) + pow(cateto2, 2));

    printf("La hipotenusa es: %.2f \n", hipotenusa);

    return 0;
}