#include<stdio.h>

int main() {
    float precio_neto, precio_total, descuento;
    printf("Ingrese el precio: ");
    scanf("%f", &precio_neto);

    descuento = precio_neto * .15;
    precio_total = precio_neto - descuento;
    printf("EL total es: %.2f", precio_total);

    printf("\n\n");
    return 0;
}