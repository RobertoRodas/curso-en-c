#include<stdio.h>

int main() {
    int n1,n2;
    printf("Ingrese dos digitos: ");
    scanf("%i %i", &n1, &n2);

    int suma = n1 + n2;
    int resta = n1 - n2;
    int mul =  n1 * n2;
    int div = n1 / n2;

    printf("\nLa suma es: %i", suma);
    printf("\nLa resta es: %i", resta);
    printf("\nLa multiplicacion es: %i", mul);
    printf("\nLa division es: %i\n", div);

    return 0;
}