#include<stdio.h>

int main() {
    int i = 0;

    while (i < 5){
        printf("While: %i\n", i);
        i++;
    }

    i = 0;
    for (i = 0; i < 5; i++) {
        printf("for: %i\n", i);
    }
    
    i = 0;
    do {
        printf("do-While: %i\n", i);
        i++;
    } while (i < 5);
    
    printf("\n");
    return 0;
}