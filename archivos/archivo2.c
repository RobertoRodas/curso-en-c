#include<stdio.h>
#include<stdlib.h>

FILE *fb1, *fb2;

int main() {

    char *dirreccion1 = "hola1.txt";
    char *dirreccion2 = "hola2.txt";      
    
    /**
     * r = read, leer
     * w = writer, escribir, para crear
     **/
    fb1 = fopen(dirreccion1, "a+");
    fb2 = fopen(dirreccion2, "a+");

    if ((fb1 == NULL) || (fb2 == NULL)) {
        printf("Los archivos no existen");
    } else {
        printf("Los archivos existen");
    }


    fclose(fb1);
    fclose(fb2);
    
    printf("\n\n");
    return 0;
}