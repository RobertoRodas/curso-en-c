#include<stdio.h>
#include<stdlib.h>

FILE *fb;
/**
 * NO funciona en linux
 */ 
int main() {
    int c;
    char *dirreccion = "hola.txt";      
    
    /**
     * r = read, leer
     * w = writer, escribir, para crear
     **/
    fb = fopen(dirreccion, "rt");

    if (fb == NULL){
        printf("Los archivos no existen");
        return 1;
    } 

    fflush(stdin);
    while ((c = fgetc(fb)) != EOF) {
        if (c == '\n') {
            printf("\n");
        } else {
            putchar(c);
        }
    }
    

    fclose(fb);
    
    printf("\n\n");
    return 0;
}