#include<stdio.h>
#include<stdlib.h>

FILE *fb;
/**
 * NO funciona en linux
 */ 
int main() {
    int c;
    char *dirreccion = "hola.txt";      
    
    /**
     * r = read, leer
     * w = writer, escribir, para crear
     **/
    fb = fopen(dirreccion, "wt");

    if (fb == NULL){
        printf("Los archivos no existen");
        return 1;
    } 

    while ((c = getchar()) != EOF) {
        fputc(c, fb);
    }
    

    fclose(fb);
    
    printf("\n\n");
    return 0;
}