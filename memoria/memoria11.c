#include<stdio.h>
#include<stdlib.h>

int main() {
    int *p;

    p = calloc(3, sizeof(int));

    for (int i = 0; i < 3; i++) {
        scanf("%i", &p[i]);
    }
    

    printf("\n\n");

    for (int i = 0; i < 3; i++) {
        printf("%i", p[i]);
    }

    printf("\n\n");
    return 0;
}