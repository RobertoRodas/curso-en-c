
#include<stdio.h>
#include<stdlib.h>
#include <time.h> 

int main() {
    int *p_array;
    int i, cantidad;

    printf("ingrese la cantidad de arregos quieres: ");
    scanf("%i", &cantidad);


    p_array = malloc(cantidad * sizeof(int));

    if (p_array == NULL) {
        printf("No hay suficiente memoria\n");
        return -1;
    } else {
        srand(time(NULL));
        for (i = 0; i < cantidad; i++) {
            p_array[i] = 1 + rand() % ((cantidad + 1 ) -1);
            printf("%i\n", p_array[i]);
        }
    }
    
    printf("\n\n");
    return 0;
}