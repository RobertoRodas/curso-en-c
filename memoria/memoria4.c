#include<stdio.h>
#include<stdlib.h>
#include <time.h> 
#define TOPE 9999999999

int main() {
    double *p_array;
    int i;

    p_array = malloc(TOPE * sizeof(double));

    if (p_array == NULL) {
        printf("No hay suficiente memoria\n");
        return -1;
    } else {
        srand(time(NULL));
        for (int i = 0; i < TOPE; i++) {
            p_array[i] = 1 + rand() % ((TOPE + 1 ) -1);
            printf("%lf\n", p_array[i]);
        }
    }
    
    printf("\n\n");
    return 0;
}